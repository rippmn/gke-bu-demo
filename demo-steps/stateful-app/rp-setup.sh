gcloud beta container backup-restore restore-plans create demo-mysql-rp \
    --location=$TF_VAR_ORIG_REGION \
    --backup-plan=projects/$TF_VAR_PROJECT_ID/locations/$TF_VAR_ORIG_REGION/backupPlans/demo-mysql-bu \
    --cluster=projects/$TF_VAR_PROJECT_ID/locations/$TF_VAR_ORIG_ZONE/clusters/orig-cluster \
    --namespaced-resource-restore-mode=delete-and-restore \
    --selected-applications=demo/demo-db-pa \
    --volume-data-restore-policy=restore-volume-data-from-backup \
    --project $TF_VAR_PROJECT_ID
