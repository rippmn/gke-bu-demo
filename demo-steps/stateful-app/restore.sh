gcloud beta container backup-restore restores create demo-mysql-restore-$(date +%Y%m%d) \
    --location=$TF_VAR_ORIG_REGION \
    --restore-plan=demo-mysql-rp \
    --backup=projects/$TF_VAR_PROJECT_ID/locations/$TF_VAR_ORIG_REGION/backupPlans/demo-mysql-bu/backups/demo-mysql-bu-$(date +%Y%m%d) \
    --project $TF_VAR_PROJECT_ID
