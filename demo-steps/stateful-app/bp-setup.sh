gcloud beta container backup-restore backup-plans create demo-mysql-bu \
--cluster=projects/$TF_VAR_PROJECT_ID/locations/$TF_VAR_ORIG_ZONE/clusters/orig-cluster \
--selected-applications=demo/demo-db-pa \
--location=$TF_VAR_ORIG_REGION \
--cron-schedule="17 6 * * *" \
--backup-retain-days=8 \
--include-volume-data \
--project $TF_VAR_PROJECT_ID

