gcloud beta container backup-restore restores delete  demo-mysql-restore-$(date +%Y%m%d)  --location $TF_VAR_ORIG_REGION  --restore-plan demo-mysql-rp --project $TF_VAR_PROJECT_ID -q 
gcloud beta container backup-restore restore-plans delete  demo-mysql-rp  --location $TF_VAR_ORIG_REGION --project $TF_VAR_PROJECT_ID -q
gcloud beta container backup-restore backups delete  demo-mysql-bu-$(date +%Y%m%d)  --location $TF_VAR_ORIG_REGION --backup-plan demo-mysql-bu --project $TF_VAR_PROJECT_ID -q
gcloud beta container backup-restore backup-plans delete  demo-mysql-bu  --location $TF_VAR_ORIG_REGION --project $TF_VAR_PROJECT_ID -q
