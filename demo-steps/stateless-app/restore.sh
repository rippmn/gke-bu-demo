gcloud beta container backup-restore restores create demo-nginx-restore-$(date +%Y%m%d) \
    --location=$TF_VAR_REST_REGION \
    --restore-plan=demo-nginx-rp \
    --backup=projects/$TF_VAR_PROJECT_ID/locations/$TF_VAR_REST_REGION/backupPlans/demo-nginx-bu/backups/demo-nginx-bu-$(date +%Y%m%d) \
    --project $TF_VAR_PROJECT_ID
