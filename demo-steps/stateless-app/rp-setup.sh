gcloud beta container backup-restore restore-plans create demo-nginx-rp \
    --location=$TF_VAR_REST_REGION \
    --backup-plan=projects/$TF_VAR_PROJECT_ID/locations/$TF_VAR_REST_REGION/backupPlans/demo-nginx-bu \
    --cluster=projects/$TF_VAR_PROJECT_ID/locations/$TF_VAR_REST_ZONE/clusters/restore-cluster \
    --namespaced-resource-restore-mode=fail-on-conflict \
    --selected-applications=demo/demo-nginx-pa \
    --project $TF_VAR_PROJECT_ID
