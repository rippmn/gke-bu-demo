gcloud beta container backup-restore backups create demo-nginx-bu-$(date +%Y%m%d) \
--location=$TF_VAR_REST_REGION \
--backup-plan=demo-nginx-bu \
--project $TF_VAR_PROJECT_ID

