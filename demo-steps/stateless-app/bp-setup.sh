gcloud beta container backup-restore backup-plans create demo-nginx-bu \
--cluster=projects/${TF_VAR_PROJECT_ID}/locations/$TF_VAR_ORIG_ZONE/clusters/orig-cluster \
--selected-applications=demo/demo-nginx-pa \
--location=$TF_VAR_REST_REGION \
--project $TF_VAR_PROJECT_ID

