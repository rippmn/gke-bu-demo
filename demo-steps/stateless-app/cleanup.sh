gcloud beta container backup-restore restores delete  demo-nginx-restore-$(date +%Y%m%d)  --location $TF_VAR_REST_REGION  --restore-plan demo-nginx-rp --project $TF_VAR_PROJECT_ID -q
gcloud beta container backup-restore restore-plans delete  demo-nginx-rp  --location $TF_VAR_REST_REGION --project $TF_VAR_PROJECT_ID -q
gcloud beta container backup-restore backups delete  demo-nginx-bu-$(date +%Y%m%d)  --location $TF_VAR_REST_REGION  --backup-plan demo-nginx-bu --project $TF_VAR_PROJECT_ID -q
gcloud beta container backup-restore backup-plans delete  demo-nginx-bu  --location $TF_VAR_REST_REGION --project $TF_VAR_PROJECT_ID -q
