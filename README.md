# Demo of backup for GKE

This project includes a set of assets demonstrate Backup for GKE. The scenarios it demos are as follows:
* cross region back and restore of a stateless app (NGinx deployment)
* in region backup of a stateful (Mysql) app and restoration after data corruption.

## Directory Structure
* **setup** - contains the steps to setup the infrastructure using terraform
  * **terraform-config** - terraform configuration files, (broken into 2 sub directories as configuration of GCP services can result errors on terraform destroy due to transitive dependencies of declared services.)
    * **project-setup** - the terraform configuration to enable the reguired services and set org policy enforcement to allow backups
    * **infra** - required infrastructure of 2 GKE clusters in different regions. (Note these clusters have public endpoints without master authorized networks, best practices would be to restrict access via a private only endpoint and/or defining master authorized networks)
  * **demo-app**
    * **k8s-manifests** - kubernetes resource definitions to create demo namespace, deploy the stateless and stateful applications, and define the protected applications to allow for backup
    * **data** - the data to use for the stateful application
* **demo-steps** - scripts to execute the Backup for GKE commands to perform backup, restore, and cleanup.
  * **stateful-app** - scripts for the stateful in region backup and restore scenario
  * **stateless-app** - scripts for the cross region backup and restore of the stateless app

## Demo Steps
### Setup 
1. After cloning project update the env-vars.sh file to reflect your GCP project id and the original and restore region environment variables. See below for a sample version (Note these are using named using the 'TF_VAR_' format so these variables can be used in the terraform infrastructure setup)

  ```
  export TF_VAR_PROJECT_ID=sample-gke-bu-demo
  export TF_VAR_ORIG_ZONE=us-west4-c
  export TF_VAR_ORIG_REGION=us-west4
  export TF_VAR_REST_ZONE=us-central1-f
  export TF_VAR_REST_REGION=us-central1
  ```
2. From a command prompt change to the setup directory and apply the env vars using the *source* command
  ```
  >source env-vars.sh
  ```
3. Change to the terraform-config/project-setup directory and initial terraform and apply the configuration. Confirm the application step at prompt or using *--auto-approve* flag.
  ```
  >terraform init
  >terraform apply 
  ```
4. Change to the *terraform-config/infra* directory and initial terraform and apply the configuration. Confirm the application step at prompt or using *--auto-approve* flag.
  ```
  >terraform init
  >terraform apply 
  ```
5. On completion of the setup change to the *setup/demo-app* directory and apply the k8s-manifests. You will need to get the cluster credentials of the orig-cluster.
```
>gcloud container clusters get-credentials orig-cluster --zone $TF_VAR_ORIG_ZONE --project $TF_VAR_PROJECT_ID
>kubectl apply -f k8s-manifests/
```
6. Watch the pods in the demo namespace to confirm the *demo-nginx-pa* and *mysql* pods are in a *Running* state.
```
>kubectl get po -n demo
```
7. Once the *mysql* pod is ready. Setup the sample data by copying the sample-data.sql file to the pod and then ssh into pod.
```
>kubectl cp data/sample-data.sql mysql-0:. -n demo
>kubectl exec -it -n demo mysql-0 -- /bin/bash
```
8. Once you are at the command line in the pod, apply the data file, confirm the table is created and populated, and exit the pod. (Note the database password is *"password"*)
```
>mysql -p < sample-data.sql
>mysql -p -e "select * from hello.hello"
>exit
```

Setup is complete

### Demo Backup of Stateful App
The following steps will backup the stateful application including persistent disk using the Backup for GKE service, corrupt the data, and then restore from the backup.

1. Change to the *demo-steps/stateful-app* directory and create the backup plan using the script containing the gcloud command. (Note this script uses environment variables setup previously)
```
>source bp-setup.sh
```
2. Take the backup of the stateful app and it's attached volume using the script container the gcloud command. (Note the backup is dynamically named using the current date. If you setup and demo on different days, be aware the dynamically generated backup name may be incorrect during the restore process)
```
>source backup.sh
```
3. ssh into the mysql container.
```
>kubectl exec -it -n demo mysql-0 -- /bin/bash
```
4. Corrupt the data by deleting the hello table. Confirm table is deleted then exit the container.
```
>mysql -p -e "drop table hello.hello"
>mysql -p -e "select * from hello.hello"
>exit
```
5. Create the restore plan using the script container the gcloud command.
```
>source rp-create.sh
```
6. Restore the stateful application using the script container the gcloud command.
```
>source restore.sh
```
7. Watch the *mysql* pod as it is redployed. When it is ready ssh into the pod.
```
>kubectl exec -it -n demo mysql-0 -- /bin/bash
```
8. Confirm the table and data have been restored and then exit the pod.
```
>mysql -p -e "select * from hello.hello"
>exit
```
9. (Optional) Clean up backup and restore using the gcloud commands in the script file.
```
>source cleanup.sh
```

### Demo Backup of Stateless App
The following steps will backup the stateless application  using the Backup for GKE service in the defined restore region, and then restore from the backup to a cluster in the restore region.

1. Change to the *demo-steps/stateless-app* directory and create the backup plan using the script containing the gcloud command. (Note this script uses environment variables setup previously)
```
>source bp-setup.sh
```
2. Take the backup of the stateless app  using the script container the gcloud command. (Note the backup is dynamically named using the current date. If you setup and demo on different days, be aware the dynamically generated backup name may be incorrect during the restore process)
```
>source backup.sh
```
5. Create the restore plan using the script container the gcloud command.
```
>source rp-create.sh
```
6. Restore the stateless application using the script container the gcloud command.
```
>source restore.sh
```
7. Get the credentials for the restore cluster in the restore region.
```
>gcloud container clusters get-credentials restore-cluster --zone $TF_VAR_REST_ZONE --project $TF_VAR_PROJECT_ID
```
8. Watch for the **demo-nginx-pa** pods in the restore cluster.
```
>kubectl get po -n demo 
```
9. (Optional) Clean up backup and restore using the gcloud commands in the script file.
```
>source cleanup.sh
```
