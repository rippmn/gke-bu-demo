
resource "google_project_organization_policy" "disable_storage_restriction" {
  project = var.PROJECT_ID
  provider = google-beta
  constraint = "compute.storageResourceUseRestrictions"

  list_policy {
    allow {
      all = true
    }
  }

}

