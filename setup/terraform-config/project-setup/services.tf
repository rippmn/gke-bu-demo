
resource "google_project_service" "svc_gke" {
  project = var.PROJECT_ID
  provider = google-beta
  service = "container.googleapis.com"
}

resource "google_project_service" "svc_cloud_monitoring" {
  project = var.PROJECT_ID
  provider = google-beta
  service = "monitoring.googleapis.com"
}
    
resource "google_project_service" "svc_cloud_cloud_trace" {
  project = var.PROJECT_ID
  provider = google-beta
  service = "cloudtrace.googleapis.com"
}

resource "google_project_service" "svc_cloud_logging" {
  project = var.PROJECT_ID
  provider = google-beta
  service = "logging.googleapis.com"
}

resource "google_project_service" "svc_iamcreds" {
  project = var.PROJECT_ID
  provider = google-beta
  service = "iamcredentials.googleapis.com"
}

resource "google_project_service" "svc_gke_backup" {
  project = var.PROJECT_ID
  provider = google-beta
  service = "gkebackup.googleapis.com"
}

