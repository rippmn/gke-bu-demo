resource "google_compute_network" "vpc_network" {
  provider = google-beta
  name = "demo-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "demo_subnet_1" {
  provider = google-beta
  name          = "vpc-demo-1"
  ip_cidr_range = "10.100.0.0/16"
  region        = var.ORIG_REGION
  network       = google_compute_network.vpc_network.id
  secondary_ip_range = [ 
    {
      range_name    = "pods"
      ip_cidr_range = "10.101.0.0/16"
    },
    {
      range_name    = "services"
      ip_cidr_range = "10.102.0.0/16"
    }
  ]
  
}

resource "google_compute_subnetwork" "demo_subnet_2" {
  provider = google-beta
  name          = "vpc-demo-2"
  ip_cidr_range = "10.103.0.0/16"
  region        = var.REST_REGION
  network       = google_compute_network.vpc_network.id
  secondary_ip_range = [ 
    {
      range_name    = "pods"
      ip_cidr_range = "10.104.0.0/16"
    },
    {
      range_name    = "services"
      ip_cidr_range = "10.105.0.0/16"
    }
  ]
  
}

