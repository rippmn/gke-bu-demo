resource "google_service_account" "demo_sa" {
  provider = google-beta
  account_id   = "demo-sa"
  display_name = "demo Service Account"
}

resource "google_project_iam_member" "demo_sa_editor" {
  provider = google-beta
  project = var.PROJECT_ID
  role    = "roles/editor"
  member =   format("%s:%s", "serviceAccount", google_service_account.demo_sa.email )
}
