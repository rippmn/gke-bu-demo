resource "google_container_cluster" "restore_cluster" {
  provider = google-beta
  name               = "restore-cluster"
  location           = var.REST_ZONE

  release_channel {
    channel = "RAPID"
  }

  initial_node_count = 2

  network = google_compute_network.vpc_network.name

  subnetwork = google_compute_subnetwork.demo_subnet_2.name

  ip_allocation_policy{
    //this block turns on vpc native mode you can specify specific subnets or cidrs for the node and services subnet, or accept the defaults here
    //note secondary ranges are returned in the order created on the subnet so this might seem a little wonky
    cluster_secondary_range_name = google_compute_subnetwork.demo_subnet_1.secondary_ip_range[0].range_name
    services_secondary_range_name = google_compute_subnetwork.demo_subnet_1.secondary_ip_range[1].range_name
  }

  node_config {
    service_account = google_service_account.demo_sa.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    image_type = "cos_containerd"
    machine_type = "e2-standard-4"
    disk_size_gb = "100"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    shielded_instance_config{
      enable_secure_boot = "true"
    }

    tags = [ "restore-cluster"]
  }

  private_cluster_config {
    enable_private_nodes = "true"
    enable_private_endpoint = "false"
    master_ipv4_cidr_block = "10.115.0.0/28"

    master_global_access_config {
      enabled = "true"
    }

  }

  workload_identity_config {
    workload_pool = "${var.PROJECT_ID}.svc.id.goog"
  }

  addons_config {
    gke_backup_agent_config {
      enabled = true
    }
  }
}


