provider "google-beta" {
  project     = var.PROJECT_ID
}


data "google_project" "project" {
    provider = google-beta
    project_id = var.PROJECT_ID
}

